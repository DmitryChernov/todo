import React, { useEffect, useState } from "react";
import ListReady from "./components/ListReady";
import getFromLocalStorage from "./components/GetFromLocalStorage";
import postToLocalStorage from "./components/PostToLocalStorage";
import Button from "./components/Button";


const App = () => {

  const [tasks, setTasks] = useState([])

  const [nameFromInput, setName] = useState('')

  useEffect(() => {
    setTasks(getFromLocalStorage)
  }, [])

  useEffect(() => {
    postToLocalStorage([...tasks])
  }, [tasks])

  const toggleStatus = (taskToggle) => {
    setTasks(prevState => prevState.map(task => {
      if (task.id === taskToggle.id) {
        return {
          ...task,
          ready: !task.ready
        }
      }
      return task
    }))
  }

  const deleteTask = (taskForDelete) => {
    setTasks(prevState => prevState.filter(task => task.id !== taskForDelete.id))
  }

  const handlerAdd = (task) => {
    setTasks(prevState => ([...prevState, task]))
    setName('')
  }

  const getNameFromInput = (value) => {
    setName(value)
  }

  const keyPress = (key) => {
    if (key.which === 13) {
      let newTask = {
        nameTask: nameFromInput,
        ready: false,
        id: tasks.length,
      }
      handlerAdd(newTask)
    }
  }

  return (
    <div className="container" id="mainContainer">
      <Button type="clear" tasks={tasks} deleteHandler={deleteTask} />
      <input className="container__input-create default_font"
        id="inputCreate"
        placeholder="Что нужно делать"
        onChange={(event) => {
          getNameFromInput(event.target.value)
        }}
        onKeyUp={(event) => {
          keyPress(event)
        }}
        value={nameFromInput}
      >
      </input>
      <Button type="add" tasks={tasks} handlerAdd={handlerAdd} name={nameFromInput} />
      <ListReady tasks={tasks} toggle={toggleStatus} deleteHandler={deleteTask} />
      <Button type="deleteReady" tasks={tasks} deleteHandler={deleteTask} />
    </div >
  )
}

// изменить ID
// настроить кнопки  

export default App