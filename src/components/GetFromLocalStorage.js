import postToLocalStorage from "./PostToLocalStorage";


function getFromLocalStorage() {

    if (localStorage.getItem('tasks') === null) {
        let tasks = [];
        postToLocalStorage(tasks);
        return JSON.parse(localStorage.getItem('tasks'));
    } else {
        return JSON.parse(localStorage.getItem('tasks'))
    }
}


export default getFromLocalStorage