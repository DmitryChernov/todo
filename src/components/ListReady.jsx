/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import Delete from '../img/delete.png'


const TaskItem = ({ task, toggle, deleteHandler, index }) => (
    <li className={task.ready ? "task font_ready" : "task default_font"}
        key={task.id}>
        {!task.ready ? `${index + 1}. ${task.nameTask}` : task.nameTask}
        <input type="checkbox" className="task__checkbox" defaultChecked={task.ready}
            onClick={() => {
                toggle(task)
            }}>
        </input>
        <img src={Delete} className="task__btn-delete-task"
            onClick={() => {
                deleteHandler(task)
            }}
        />
    </li>)
const ListReady = ({ tasks, toggle, deleteHandler }) => {

    const tasksReady = tasks.filter(task => task.ready)

    const tasksNoReady = tasks.filter(task => !task.ready)

    const listItems = tasksNoReady.map((task, index) => {
        return (
            <TaskItem task={task} toggle={toggle} deleteHandler={deleteHandler} index={index} />
        )
    })

    const listItemsReady = tasksReady.map((task) => {
        return (
            <TaskItem task={task} toggle={toggle} deleteHandler={deleteHandler} />
        )
    })

    return (
        <>
            <ul className="list" id="list">
                {listItems}
            </ul>

            <ul className="list" id="list">
                {listItemsReady}
            </ul>
        </>
    )
}

export default ListReady