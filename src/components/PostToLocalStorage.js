
function postToLocalStorage(tasks) {
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

export default postToLocalStorage