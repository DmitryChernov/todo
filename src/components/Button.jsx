import React from 'react'

const Button = ({ type, tasks, handlerAdd, name, deleteHandler }) => {

    const onClickAdd = () => {
        let newTask = {
            nameTask: name,
            ready: false,
            id: tasks.length,
        }
        handlerAdd(newTask)
    }

    const onClickDeleteReady = () => {
        tasks.forEach((task) => {
            if (task.ready) {
                deleteHandler(task)
            }
        })
    }

    const onClickDeleteAll = () => {
        tasks.forEach((task) => {
            deleteHandler(task)
        })
    }

    if (type === "clear") {
        return (
            <span
                className='container__btn-update default_btn default_font'
                onClick={() => {
                    onClickDeleteAll()
                }}
                id='btnUpdate'>
                Начать новый список
            </span>
        )
    }

    if (type === "add") {
        return (
            <span
                className='container__btn-add default_btn default_font'
                onClick={() => {
                    onClickAdd()
                }}
                id='btnAdd'>
                Enter
            </span>
        )
    }

    if (type === "deleteReady") {
        return (
            <span className="container__btn-delete default_font default_btn"
                id="btnDeleteReadyTask"
                onClick={
                    onClickDeleteReady
                }>
                Удалить выполненные
            </span>
        )
    }
}

export default Button

